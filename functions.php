<?php
error_reporting( error_reporting() & ~E_NOTICE );
session_start();
$MENU_ITEMS = array('Sākums', 'Reģistrācija', 'Kontakti');
$page_id = $_GET['id'];
$title = $MENU_ITEMS[intval($page_id) - 1];

function redirect_to($url)
{
  header('Location: ' . $url);
}

// 11.3
function check_user()
{
  if ($_POST['username'] == 'Armands') {
    $_SESSION['user'] = $_POST['username'];
    unset($_POST['username']);
    redirect_to('/index.php?id=1');
  } else {
    redirect_to('/login.php?not-found');
  }
}

// 11.4
function get_content($id)
{
  if ($id == '1')
    require 'sakums.php';
  if ($id == '2')
    require 'registracija.php';
  if ($id == '3')
    require 'autors.php';
}

// 11.5
function check_registration_and_save()
{
  if (!$_POST['name'] || !$_POST['kind'] || !$_POST['gender'] || !$_POST['chempion'] || !$_POST['registree']) {
    echo '<div class="error right">Rģistracijas forma nav aizpildīta.</div>';
    return;
  }

  $txt = $_POST['name'] . ', ' . $_POST['kind'] . ', ' . $_POST['gender'] . ', ' . $_POST['chempion'] . ', ' . $_POST['registree'] . "\n";
  file_put_contents('dogs.txt', $txt, FILE_APPEND);

  unset($_POST['name']);
  unset($_POST['kind']);
  unset($_POST['gender']);
  unset($_POST['chempion']);
  unset($_POST['registree']);
}
