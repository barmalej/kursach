<?php
require_once 'functions.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
  check_user();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link rel="stylesheet" href="/css/login.css">
</head>

<body>
  <form action="/login.php" method="POST">
    <div class="login-inputs">
      <div class="container">
        <div class="container-cell-1">
          <label for="user-name">Lietotājs:</label>
        </div>
        <div class="container-cell-2">
          <input id="user-name" name="username" type="text" />
          <?php if (isset($_GET['not-found'])) echo '<div class="error">Lietotājs nav zināms</div>' ?>
        </div>
      </div>
      <div class="container">
        <div class="container-cell-2">
          <input type="submit" value="OK">
        </div>
      </div>
    </div>
  </form>
</body>

</html>