<article>
  <h1>Izskats un īpašības</h1>
  <section>
    <h2>Redze</h2>
    <p>Suņa redze ir veidota tā, lai kalpotu sunim kā medniekam. Suņa redze nosacīti ir slikta, tās asums ir vājāks kā cilvēkam, toties spēja saskatīt kustīgu objektu ir ļoti augsta. Ir pierādīts, ka suņi spēj saskatīt un identificēt savu saimnieku no 1,6 kilometru attāluma (vienas jūdzes attāluma). Kā krēslas stundu mednieki suņi spēj baltstīties uz savu redzi slikta apgaismojuma situācijā, tiem ir lielas acu zīlītes, kas uztver visniecīgāko gaismu. Kā lielākā daļa zīdītāju suns redz tikai divas krāsas, tā redze ir līdzīga cilvēkam - daltoniķim, kurš neredz visas krāsas</p>
  </section>
  <section>
    <h2>Dzirde</h2>
    <p>Suņa dzirde spēj uztvert skaņas 40 Hz - 60,000 Hz diapozonā, tas nozīmē, ka abas diapozona robežas pārsniedz cilvēka dzirdes iespējas. Sunim ausis ir kustīgas, līdz ar to tas spēj, pagriežot ausi skaņas virzienā, precīzi noteikt skaņas avotu. Suņa ausi vada 18 vai vairāk muskuļi, kas spēj ausi pacelt, pagriezt, sašķiebt, nolaist. Suns ātrāk par cilvēku nosaka skaņas avotu, kā arī spēja sadzirdēt skaņu no tāluma ir 4x labāka kā cilvēkam.</p>
  </section>
  <section>
    <h2>Oža</h2>
    <p>Ja cilvēkam galvenais informācijas avots ir redze, tad sunim tā ir oža. Suņa smadzenēs sektors, kas apstrādā smaržu informāciju ir 40x lielāks nekā cilvēkam, kas sastāv no apmēram 125 - 220 miljoniem smaržas jūtīgiem receptoriem. Bladhaundam ir pat līdz 300 miljoniem receptoru. Salīdzinot ar cilvēku, suns spēj sajust smaržu, kas ir 100 miljonus reižu vājāka, nekā to saož cilvēks.</p>
  </section>
</article>