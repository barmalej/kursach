<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') check_registration_and_save();
?>
<form action="/index.php?id=2" method="POST" class="form-container" id="registration-form">
  <h1>Reģistrācija</h1>
  <fieldset>
    <legend>Suņa dati</legend>
    <div class="container" style="margin-bottom: 15px;">
      <div class="container-cell-1">
        <div class="container padded">
          <div class="container-cell-1">
            <label for="name">Suņa vārds:</label>
          </div>
          <div class="container-cell-2">
            <input id="name" name="name" type="text" maxlength="15" value="<?php echo $_POST['name'] ?>" />
          </div>
        </div>
      </div>
      <div class="container-cell-2">
        <div class="container padded">
          <div class="container-cell-1">
            <label for="kind">Šķirne:</label>
          </div>
          <div class="container-cell-2">
            <input id="kind" name="kind" type="text" maxlength="15" value="<?php echo $_POST['kind'] ?>" />
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="container-cell-1">
        <div class="container padded">
          <div class="container-cell-1">
            <label for="gender">Dzimums</label>
          </div>
          <div class="container-cell-2">
            <select name="gender" id="gender">
              <option value="male" <?php if ($_POST['gender'] == 'male') echo 'selected' ?>>Viriešu</option>
              <option value="female" <?php if ($_POST['gender'] == 'female') echo 'selected' ?>>Sieviešu</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset>
    <legend>Dati par sertifikātiem</legend>
    <div>Vai Jūsu suns ir Latvijas čempions?</div>
    <div class="container" style="width: 60%;">
      <div class="container-cell-1">
        <div class="container">
          <div class="container-cell-1">
            <input type="radio" name="chempion" value="yes" id="yes" <?php if ($_POST['chempion'] == 'yes' || !isset($_POST['chempion'])) echo 'checked' ?>>
          </div>
          <div class="container-cell-2">
            <label for="yes">Jā</label>
          </div>
        </div>
      </div>
      <div class="container-cell-2">
        <div class="container">
          <div class="container-cell-1">
            <input type="radio" name="chempion" value="no" id="no" <?php if ($_POST['chempion'] == 'no') echo 'checked' ?>>
          </div>
          <div class="container-cell-2">
            <label for="no">Nē</label>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset>
    <legend>Dati par reģistrētāju</legend>
    <div class="container">
      <div class="container-cell-1">
        <div class="container">
          <div class="contaier-cell-1">
            <label for="registree">Datus ievadīja:</label>
          </div>
          <div class="container-cell-2">
            <input type="text" name="registree" id="registree" value="<?php if ($_POST['registree']) echo $_POST['registree'];
                                                                      else echo 'Armands' ?>">
          </div>
        </div>
      </div>
    </div>
  </fieldset>
  <div class="confirm">
    <button type="submit" class="confirm-button">Labi</button>
    <button class="confirm-button">Atcelt</button>
  </div>
</form>
<script type="text/javascript">
  const form = document.getElementById('registration-form');
  form.addEventListener('submit', validate);
</script>