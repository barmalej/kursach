function getNowDateString() {
  const date = new Date();
  const weekday = date.toLocaleDateString('lv-LV', { weekday: 'long' }).toLowerCase();
  const year = date.getFullYear();
  const day = date.getDate();
  const month = date.toLocaleDateString('lv-LV', { month: 'long' }).toLowerCase();

  return `Šodien ir ${weekday}, ${year}. gada ${day}. ${month}`;
}

function validate(e) {
  e.preventDefault();
  const form = document.getElementById('registration-form');
  const nameInput = document.getElementById('name');
  const kindInput = document.getElementById('kind');

  if(nameInput.value.length < 2 || nameInput.value.length > 12) {
    alert('Suņa vardam jābūt vairāk pa 1 un mazāk pa 12 sumboliem');
    return;
  }

  if (typeof kindInput.value != 'string' || kindInput.value.length === 0) {
    alert('Ievadiet suņa šķirni');
    return;
  }

  form.submit();
}

function openGoogle() {
  if(confirm('Vai atvērt google.com?')) {
    window.open('http://www.google.com', 'Google meklēšana', 'width=800, height=600, top=150, left=150');
  }
} 
