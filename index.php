<?php 
require_once "functions.php";
// unset($_SESSION['user']);
if (empty($_SESSION['user'])) {
  redirect_to('login.php');
}
 ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="author" content="Armands">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $title ?></title>
  <link rel="stylesheet" href="css/index.css">
  <script src="js/main.js"></script>
</head>

<body>
  <div class="page-container">
    <div class="content-container right">
      <div class="date" id="date"></div>
      <script type="text/javascript">
        document.getElementById('date').innerHTML = getNowDateString();
      </script>
    </div>
    <div class="side-bar">
      <ul>
        <?php
        for ($i = 1; $i <= count($MENU_ITEMS); $i++) {
          $menu_class = $page_id == $i ? 'selected' : '';
          echo '<li><a class="' . $menu_class . '" href="/index.php?id=' . $i . '">' . $MENU_ITEMS[$i - 1] . '</a></li>';
        }
        ?>
      </ul>
      <div style="width: 90%;">
        <img width="100%" src="suns.jpeg" alt="suns" />
        <button onclick="openGoogle()" class="google">Meklēt vairāk informāciju</button>
      </div>
    </div>
    <div class="content-container">
      <?php get_content($page_id) ?>
    </div>
  </div>
</body>

</html>